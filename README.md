# Sistema para la Administración de Datos Geográficos
Este es un proyecto colaborativo para realizar un sistema web que permita la administración de datos geográficos, en donde se puedan crear, actualizar, consultar y eliminar capas de información de tipo vectorial (punto, línea, polígono) para la modelación territorial como por ejemplo la modelación de una ciudad la cual puede ser conformada por varios elementos espaciales como:
- Vialidades - _Línea_
- Manzanas - _Polígono_
- Predios - _Polígono_
- Construcciones - _Polígono_
- Alumbrados - _Punto_
- Áreas verdes - _Polígono_
- Camellones - _Polígono_
- Alcantarillas - _Punto_
- Entre otros

## Modularización del Sistema
El desarrollo del sistema se realizará por módulos los cuales al integrarlos formarán la estructura base para poder escalar a otro tipo de requerimientos, el sistema se conformará por los siguientes módulos:
- Módulo de Administración de Usuarios.
- Módulo de Consultas y Reportes.
- Módulo de Creación Cartográfica (Dibujo en el mapa).
- Módulo de Exportación de los datos.
- Módulo de Auditoria de los recursos (Histórico).

### Módulo de Administración de Usuarios
El sistema debe contar con un módulo de administración de usuarios que permita la creación, actualización y eliminación de usuarios, grupos, roles y permisos para el control de los recursos geográficos y tabulares (catálogos).

#### Especificaciones
##### Usuarios
1. Para acceder al sistema se necesitará un usuario y contraseña proporcionado por un **administrador** , que en caso de olvidar se pueda recuperar enviando los datos por correo electrónico. 
2. Un usuario puede tener uno o muchos roles.
3. Un usuario puede pertenecer a un grupo o a ninguno.
4. Un usuario debe tener un estatus para conocer si está activo o inactivo.

##### Grupos
1. Un grupo permitirá agrupar a dos o más usuarios.
2. Un grupo puede tener uno o muchos roles.
3. Un grupo debe tener un estatus para conocer si está activo o inactivo.

##### Roles
1. Los roles permitirán tener el control de los recursos (tablas) mediante permisos de inserción, actualización, lectura y eliminación (CRUD).
2. Los roles básicos para el sistema son los siguientes:
    - SuperAdmin
    - Admin
    - Editor
    - Viewer

##### Recursos
1. Los recursos son las tablas de la base de datos a las cuales se les otorgará muchos permisos de acuerdo al roles que tenga el usuario o grupo
2. Los recursos se clasifican en dos tipos _Geográficos_ y _Catálogos_
3. Los roles **SuperAdmin** y **Admin** podrán crear y eliminar los recursos del sistema.
4. Los demás roles solo podrán consultar o editar en caso de ser necesario. 

##### Roles - Recursos
El sistema tendrá en un inicio los siguientes permisos a los recursos de acuerdo a los siguientes roles:

Nomenclatura:
- Insertar = I
- Actualizar = A
- Eliminar = E
- Consultar = C

|**Recurso/Rol**|**Users**| **DataUsers**|**Groups**|**Roles**|**Status**|**Resources**|
|---|---|---|---|---|---|---|
|**SuperAdmin**|I, A, E, C|I, A, E, C|I, A, E, C|I, A, E, C|I, A, E, C|I, A, E, C|
|**Admin**|I, A, E, C|I, A, E, C|I, A, C|I, A, C|C|I, A, E, C|
|**Editor**|A, c|A, C|C|C|C|C|
|**Viewer**|C|C|C|-|-|-|

