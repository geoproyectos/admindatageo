--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: group_role; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE group_role (
    id integer NOT NULL,
    group_id integer NOT NULL,
    role_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE group_role OWNER TO admingeo;

--
-- Name: TABLE group_role; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE group_role IS 'Tabla pivote entre Groups y Roles con relación de muchos a muchos';


--
-- Name: COLUMN group_role.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN group_role.id IS 'Clave principal de la entidad Group_Role';


--
-- Name: COLUMN group_role.group_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN group_role.group_id IS 'Id del grupo (Groups)';


--
-- Name: COLUMN group_role.role_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN group_role.role_id IS 'Id del role (Roles)';


--
-- Name: COLUMN group_role.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN group_role.created IS 'Identifica la fecha de cuando se le asigno un role a un grupo';


--
-- Name: COLUMN group_role.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN group_role.updated IS 'Identifica la fecha de actualización del role de un grupo';


--
-- Name: group_role_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE group_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE group_role_id_seq OWNER TO admingeo;

--
-- Name: group_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE group_role_id_seq OWNED BY group_role.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE groups (
    id integer NOT NULL,
    groupname character varying(50) NOT NULL,
    groupdesc character varying(250) DEFAULT NULL::character varying,
    profile_pic character varying(100) DEFAULT NULL::character varying,
    status boolean DEFAULT true NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE groups OWNER TO admingeo;

--
-- Name: TABLE groups; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE groups IS 'Es la entidad de Grupos del sistema';


--
-- Name: COLUMN groups.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN groups.id IS 'Clave principal de la entidad Groups';


--
-- Name: COLUMN groups.groupname; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN groups.groupname IS 'Nombre del grupo';


--
-- Name: COLUMN groups.groupdesc; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN groups.groupdesc IS 'Descripción del grupo';


--
-- Name: COLUMN groups.profile_pic; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN groups.profile_pic IS 'Ruta de la imagen del perfil del grupo';


--
-- Name: COLUMN groups.status; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN groups.status IS 'Estatus del grupo (activo o inactivo)';


--
-- Name: COLUMN groups.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN groups.created IS 'Identifica la fecha de creación del grupo';


--
-- Name: COLUMN groups.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN groups.updated IS 'Identifica la fecha de actualización del grupo';


--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groups_id_seq OWNER TO admingeo;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: resource_role; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE resource_role (
    id integer NOT NULL,
    resource_id integer NOT NULL,
    role_id integer NOT NULL,
    can_add boolean DEFAULT false NOT NULL,
    can_view boolean DEFAULT false NOT NULL,
    can_edit boolean DEFAULT false NOT NULL,
    can_delete boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE resource_role OWNER TO admingeo;

--
-- Name: TABLE resource_role; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE resource_role IS 'Tabla pivote entre Resources y Roles con relación de muchos a muchos';


--
-- Name: COLUMN resource_role.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.id IS 'Clave principal de la entidad Resource_Role';


--
-- Name: COLUMN resource_role.resource_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.resource_id IS 'Id del recurso (Resources)';


--
-- Name: COLUMN resource_role.role_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.role_id IS 'Id del role (Roles)';


--
-- Name: COLUMN resource_role.can_add; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.can_add IS 'Permiso para insertar registros al recurso (Tabla)';


--
-- Name: COLUMN resource_role.can_view; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.can_view IS 'Permiso para consultar resgistros del recurso (Tabla)';


--
-- Name: COLUMN resource_role.can_edit; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.can_edit IS 'Permiso para actualizar registros del recurso (Tabla)';


--
-- Name: COLUMN resource_role.can_delete; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.can_delete IS 'Permiso para eliminar registros del recurso (Tabla)';


--
-- Name: COLUMN resource_role.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.created IS 'Identifica la fecha cuando a un role se le dieron permisos a un recurso';


--
-- Name: COLUMN resource_role.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resource_role.updated IS 'Identifica la fecha de actualización de los permisos de un recurso que tiene un role';


--
-- Name: resource_role_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE resource_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE resource_role_id_seq OWNER TO admingeo;

--
-- Name: resource_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE resource_role_id_seq OWNED BY resource_role.id;


--
-- Name: resources; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE resources (
    id integer NOT NULL,
    resourcename character varying(50) NOT NULL,
    resourcedesc character varying(250) DEFAULT NULL::character varying,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE resources OWNER TO admingeo;

--
-- Name: TABLE resources; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE resources IS 'Identifica los Recursos del sistema (Tablas)';


--
-- Name: COLUMN resources.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resources.id IS 'Clave principal de la entidad Resources';


--
-- Name: COLUMN resources.resourcename; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resources.resourcename IS 'Nombre del recurso';


--
-- Name: COLUMN resources.resourcedesc; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resources.resourcedesc IS 'Descripción del recurso';


--
-- Name: COLUMN resources.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resources.created IS 'Identifica la fecha de creación del recurso';


--
-- Name: COLUMN resources.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN resources.updated IS 'Identifica la fecha de actualización del recurso';


--
-- Name: resources_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE resources_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE resources_id_seq OWNER TO admingeo;

--
-- Name: resources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE resources_id_seq OWNED BY resources.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE roles (
    id integer NOT NULL,
    rolename character varying(50) NOT NULL,
    roledesc character varying(250) DEFAULT NULL::character varying,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE roles OWNER TO admingeo;

--
-- Name: TABLE roles; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE roles IS 'Es la entidad de Roles del sistema';


--
-- Name: COLUMN roles.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN roles.id IS 'Clave principal de la entidad Roles';


--
-- Name: COLUMN roles.rolename; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN roles.rolename IS 'Nombre del role';


--
-- Name: COLUMN roles.roledesc; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN roles.roledesc IS 'Descripción del role';


--
-- Name: COLUMN roles.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN roles.created IS 'Identifica la fecha de creación del role';


--
-- Name: COLUMN roles.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN roles.updated IS 'Identifica la fecha de actualización del role';


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_id_seq OWNER TO admingeo;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: user_group; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE user_group (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE user_group OWNER TO admingeo;

--
-- Name: TABLE user_group; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE user_group IS 'Tabla pivote entre Users y Groups con relación de muchos a muchos';


--
-- Name: COLUMN user_group.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_group.id IS 'Clave principal de la entidad User_Group';


--
-- Name: COLUMN user_group.user_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_group.user_id IS 'Id del usuario (Users)';


--
-- Name: COLUMN user_group.group_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_group.group_id IS 'Id del grupo (groups)';


--
-- Name: COLUMN user_group.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_group.created IS 'Identifica la fecha de cuando se agrego un usuario a un grupo';


--
-- Name: COLUMN user_group.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_group.updated IS 'Identifica la fecha de actualización de un usuario a un grupo';


--
-- Name: user_group_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE user_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_group_id_seq OWNER TO admingeo;

--
-- Name: user_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE user_group_id_seq OWNED BY user_group.id;


--
-- Name: user_role; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE user_role (
    id integer NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE user_role OWNER TO admingeo;

--
-- Name: TABLE user_role; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE user_role IS 'Tabla pivote entre Users y Roles con relación de muchos a muchos';


--
-- Name: COLUMN user_role.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_role.id IS 'Clave principal de la entidad User_Role';


--
-- Name: COLUMN user_role.user_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_role.user_id IS 'Id del usuario (Users)';


--
-- Name: COLUMN user_role.role_id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_role.role_id IS 'Id del role (Roles)';


--
-- Name: COLUMN user_role.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_role.created IS 'Identifica la fecha de cuando se le asigno un role a un usuario';


--
-- Name: COLUMN user_role.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN user_role.updated IS 'Identifica la fecha de actualización del role de un usuario';


--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_role_id_seq OWNER TO admingeo;

--
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE user_role_id_seq OWNED BY user_role.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: admingeo
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    ap character varying(50) NOT NULL,
    am character varying(50) NOT NULL,
    password character varying(250) NOT NULL,
    email character varying(100) NOT NULL,
    profile_pic character varying(100) DEFAULT NULL::character varying,
    status boolean DEFAULT true NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    updated timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE users OWNER TO admingeo;

--
-- Name: TABLE users; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON TABLE users IS 'Es la entidad de Usuarios del sistema';


--
-- Name: COLUMN users.id; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.id IS 'Clave principal de la entidad Users';


--
-- Name: COLUMN users.name; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.name IS 'Nombre del usuario';


--
-- Name: COLUMN users.ap; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.ap IS 'Apellido paterno del usuario';


--
-- Name: COLUMN users.am; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.am IS 'Apellido materno del usuario';


--
-- Name: COLUMN users.password; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.password IS 'Contraseña del usuario';


--
-- Name: COLUMN users.email; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.email IS 'Email del usuario';


--
-- Name: COLUMN users.profile_pic; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.profile_pic IS 'Ruta de la imagen del perfil de usuario';


--
-- Name: COLUMN users.status; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.status IS 'Estatus del usuario (activo o inactivo)';


--
-- Name: COLUMN users.created; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.created IS 'Identifica la fecha de creación del usuario';


--
-- Name: COLUMN users.updated; Type: COMMENT; Schema: public; Owner: admingeo
--

COMMENT ON COLUMN users.updated IS 'Identifica la fecha de actualización del usuario';


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: admingeo
--

CREATE SEQUENCE users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO admingeo;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admingeo
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: group_role id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY group_role ALTER COLUMN id SET DEFAULT nextval('group_role_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: resource_role id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resource_role ALTER COLUMN id SET DEFAULT nextval('resource_role_id_seq'::regclass);


--
-- Name: resources id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resources ALTER COLUMN id SET DEFAULT nextval('resources_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: user_group id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_group ALTER COLUMN id SET DEFAULT nextval('user_group_id_seq'::regclass);


--
-- Name: user_role id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_role ALTER COLUMN id SET DEFAULT nextval('user_role_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: group_role; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY group_role (id, group_id, role_id, created, updated) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY groups (id, groupname, groupdesc, profile_pic, status, created, updated) FROM stdin;
\.


--
-- Data for Name: resource_role; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY resource_role (id, resource_id, role_id, can_add, can_view, can_edit, can_delete, created, updated) FROM stdin;
\.


--
-- Data for Name: resources; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY resources (id, resourcename, resourcedesc, created, updated) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY roles (id, rolename, roledesc, created, updated) FROM stdin;
\.


--
-- Data for Name: user_group; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY user_group (id, user_id, group_id, created, updated) FROM stdin;
\.


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY user_role (id, user_id, role_id, created, updated) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: admingeo
--

COPY users (id, name, ap, am, password, email, profile_pic, status, created, updated) FROM stdin;
\.


--
-- Name: group_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('group_role_id_seq', 1, false);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('groups_id_seq', 1, false);


--
-- Name: resource_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('resource_role_id_seq', 1, false);


--
-- Name: resources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('resources_id_seq', 1, false);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('roles_id_seq', 1, false);


--
-- Name: user_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('user_group_id_seq', 1, false);


--
-- Name: user_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('user_role_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: admingeo
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- Name: group_role group_role_group_id_role_id_uk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT group_role_group_id_role_id_uk UNIQUE (group_id, role_id);


--
-- Name: group_role group_role_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT group_role_id_pk PRIMARY KEY (id);


--
-- Name: groups groups_groupname_uk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_groupname_uk UNIQUE (groupname);


--
-- Name: groups groups_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_id_pk PRIMARY KEY (id);


--
-- Name: resource_role resource_role_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resource_role
    ADD CONSTRAINT resource_role_id_pk PRIMARY KEY (id);


--
-- Name: resource_role resource_role_resource_id_role_id_uk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resource_role
    ADD CONSTRAINT resource_role_resource_id_role_id_uk UNIQUE (resource_id, role_id);


--
-- Name: resources resources_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resources
    ADD CONSTRAINT resources_id_pk PRIMARY KEY (id);


--
-- Name: resources resources_resourcename_uk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resources
    ADD CONSTRAINT resources_resourcename_uk UNIQUE (resourcename);


--
-- Name: roles roles_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_id_pk PRIMARY KEY (id);


--
-- Name: user_group user_group_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_group_id_pk PRIMARY KEY (id);


--
-- Name: user_group user_group_user_id_group_id_uk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_group_user_id_group_id_uk UNIQUE (user_id, group_id);


--
-- Name: user_role user_role_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_id_pk PRIMARY KEY (id);


--
-- Name: user_role user_role_user_id_role_id_uk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_user_id_role_id_uk UNIQUE (user_id, role_id);


--
-- Name: users users_email_uk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_uk UNIQUE (email);


--
-- Name: users users_id_pk; Type: CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_id_pk PRIMARY KEY (id);


--
-- Name: group_role group_role_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT group_role_group_id_fk FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: group_role group_role_role_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY group_role
    ADD CONSTRAINT group_role_role_id_fk FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: resource_role resource_role_resource_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resource_role
    ADD CONSTRAINT resource_role_resource_id_fk FOREIGN KEY (resource_id) REFERENCES resources(id);


--
-- Name: resource_role resource_role_role_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY resource_role
    ADD CONSTRAINT resource_role_role_id_fk FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: user_group user_group_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_group_group_id_fk FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: user_group user_group_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_group
    ADD CONSTRAINT user_group_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: user_role user_role_role_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_role_id_fk FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- Name: user_role user_role_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: admingeo
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- PostgreSQL database dump complete
--

