CREATE TABLE users (
    id SERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    ap VARCHAR(50) NOT NULL,
    am VARCHAR(50) NOT NULL,
    password VARCHAR(250) NOT NULL,
    email VARCHAR(100) NOT NULL,
    profile_pic VARCHAR(100) DEFAULT NULL,
    status BOOLEAN DEFAULT TRUE NOT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT users_id_pk PRIMARY KEY (id),
    CONSTRAINT users_email_uk UNIQUE (email)
);

COMMENT ON TABLE users IS 'Es la entidad de Usuarios del sistema';

COMMENT ON COLUMN users.id IS 'Clave principal de la entidad Users';
COMMENT ON COLUMN users.name IS 'Nombre del usuario';
COMMENT ON COLUMN users.ap IS 'Apellido paterno del usuario';
COMMENT ON COLUMN users.am IS 'Apellido materno del usuario';
COMMENT ON COLUMN users.password IS 'Contraseña del usuario';
COMMENT ON COLUMN users.email IS 'Email del usuario';
COMMENT ON COLUMN users.profile_pic IS 'Ruta de la imagen del perfil de usuario';
COMMENT ON COLUMN users.status IS 'Estatus del usuario (activo o inactivo)';
COMMENT ON COLUMN users.created IS 'Identifica la fecha de creación del usuario';
COMMENT ON COLUMN users.updated IS 'Identifica la fecha de actualización del usuario';