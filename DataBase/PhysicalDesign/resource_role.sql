CREATE TABLE resource_role (
    id SERIAL NOT NULL,
    resource_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,
    can_add BOOLEAN DEFAULT FALSE NOT NULL,
    can_view BOOLEAN DEFAULT FALSE NOT NULL,
    can_edit BOOLEAN DEFAULT FALSE NOT NULL,
    can_delete BOOLEAN DEFAULT FALSE NOT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT resource_role_id_pk PRIMARY KEY (id),
    CONSTRAINT resource_role_resource_id_fk FOREIGN KEY (resource_id)
        REFERENCES resources(id),
    CONSTRAINT resource_role_role_id_fk FOREIGN KEY (role_id)
        REFERENCES roles(id),
    CONSTRAINT resource_role_resource_id_role_id_uk UNIQUE (resource_id, role_id)
);

COMMENT ON TABLE resource_role IS 'Tabla pivote entre Resources y Roles con relación de muchos a muchos';

COMMENT ON COLUMN resource_role.id IS 'Clave principal de la entidad Resource_Role';
COMMENT ON COLUMN resource_role.resource_id IS 'Id del recurso (Resources)';
COMMENT ON COLUMN resource_role.can_add IS 'Permiso para insertar registros al recurso (Tabla)';
COMMENT ON COLUMN resource_role.can_view IS 'Permiso para consultar resgistros del recurso (Tabla)';
COMMENT ON COLUMN resource_role.can_edit IS 'Permiso para actualizar registros del recurso (Tabla)';
COMMENT ON COLUMN resource_role.can_delete IS 'Permiso para eliminar registros del recurso (Tabla)';
COMMENT ON COLUMN resource_role.role_id IS 'Id del role (Roles)';
COMMENT ON COLUMN resource_role.created IS 'Identifica la fecha cuando a un role se le dieron permisos a un recurso';
COMMENT ON COLUMN resource_role.updated IS 'Identifica la fecha de actualización de los permisos de un recurso que tiene un role';