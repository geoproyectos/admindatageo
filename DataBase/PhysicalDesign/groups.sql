CREATE TABLE groups(
    id SERIAL NOT NULL,
    groupname VARCHAR(50) NOT NULL,
    groupdesc VARCHAR(250) DEFAULT NULL,
    profile_pic VARCHAR(100) DEFAULT NULL,
    status BOOLEAN DEFAULT TRUE NOT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT groups_id_pk PRIMARY KEY (id),
    CONSTRAINT groups_groupname_uk UNIQUE (groupname)
);

COMMENT ON TABLE groups IS 'Es la entidad de Grupos del sistema';

COMMENT ON COLUMN groups.id IS 'Clave principal de la entidad Groups';
COMMENT ON COLUMN groups.groupname IS 'Nombre del grupo';
COMMENT ON COLUMN groups.groupdesc IS 'Descripción del grupo';
COMMENT ON COLUMN groups.profile_pic IS 'Ruta de la imagen del perfil del grupo';
COMMENT ON COLUMN groups.status IS 'Estatus del grupo (activo o inactivo)';
COMMENT ON COLUMN groups.created IS 'Identifica la fecha de creación del grupo';
COMMENT ON COLUMN groups.updated IS 'Identifica la fecha de actualización del grupo';