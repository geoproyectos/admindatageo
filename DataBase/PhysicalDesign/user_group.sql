CREATE TABLE user_group(
    id SERIAL NOT NULL,
    user_id INTEGER NOT NULL,
    group_id INTEGER NOT NULL,
    created TIMESTAMP DEFAULT now() NOT NULL,
    updated TIMESTAMP DEFAULT now() NOT NULL,
    CONSTRAINT user_group_id_pk PRIMARY KEY (id),
    CONSTRAINT user_group_user_id_fk FOREIGN KEY (user_id)
        REFERENCES users(id),
    CONSTRAINT user_group_group_id_fk FOREIGN KEY (group_id)
        REFERENCES groups(id),
    CONSTRAINT user_group_user_id_group_id_uk UNIQUE (user_id, group_id)
);

COMMENT ON TABLE user_group IS 'Tabla pivote entre Users y Groups con relación de muchos a muchos';

COMMENT ON COLUMN user_group.id IS 'Clave principal de la entidad User_Group';
COMMENT ON COLUMN user_group.user_id IS 'Id del usuario (Users)';
COMMENT ON COLUMN user_group.group_id IS 'Id del grupo (groups)';
COMMENT ON COLUMN user_group.created IS 'Identifica la fecha de cuando se agrego un usuario a un grupo';
COMMENT ON COLUMN user_group.updated IS 'Identifica la fecha de actualización de un usuario a un grupo';