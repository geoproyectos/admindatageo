# Modelo Relacional del Sistema de Administración de Datos Geográficos

## USERS
Es la entidad de Usuarios del sistema.

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad Users|
|name|Varchar|50|-|No|-|Nombre del usuario|
|ap|Varchar|50|-|No|-|Apellido paterno del usuario|
|am|Varchar|50|-|No|-|Apellido materno del usuario|
|password|Varchar|250|-|No|-|Contraseña del usuario en Sha-3|
|email|Varchar|100|UK|No|-|Email del usuario|
|profile_pic|Varchar|100|-|Si|Null|Ruta de la imagen del perfil de usuario|
|status|Boolean|-|-|No|True|Estatus del usuario (activo o inactivo)|
|created|Timestamp|-|-|No|now()|Identifica la fecha de creación del usuario|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización del usuario|

## GROUPS
Es la entidad de Grupos del sistema.

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad Groups|
|groupname|Varchar|50|UK|No|-|Nombre del grupo|
|groupdesc|Varchar|250|-|Si|Null|Descripción del grupo|
|profile_pic|Varchar|100|-|Si|Null|Ruta de la imagen del perfil del grupo|
|status|Boolean|-|-|No|True|Estatus del grupo (activo o inactivo)|
|created|Timestamp|-|-|No|now()|Identifica la fecha de creación del grupo|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización del grupo|

## ROLES
Es la entidad de Roles del sistema.

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad Roles|
|rolename|Varchar|50|UK|No|-|Nombre del role|
|roledesc|Varchar|250|-|Si|Null|Descripción del role|
|created|Timestamp|-|-|No|now()|Identifica la fecha de creación del role|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización del role|

## RESOURCES
Identifica los Recursos del sistema (Tablas).

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad Resources|
|resourcename|Varchar|50|UK|No|-|Nombre del recurso|
|resourcedesc|Varchar|250|-|Si|Null|Descripción del recurso|
|created|Timestamp|-|-|No|now()|Identifica la fecha de creación del recurso|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización del recurso|

## User_Group
Tabla pivote entre Users y Groups con relación de muchos a muchos.

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad User_Group|
|user_id|Integer|-|UK, FK|No|-|Id del usuario (Users)|
|group_id|Integer|-|UK, FK|No|-|Id del grupo (Groups)|
|created|Timestamp|-|-|No|now()|Identifica la fecha de cuando se agrego un usuario a un grupo|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización de un usuario a un grupo|

## User_Role
Tabla pivote entre Users y Roles con relación de muchos a muchos.

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad User_Role|
|user_id|Integer|-|UK, FK|No|-|Id del usuario (Users)|
|role_id|Integer|-|UK, FK|No|-|Id del role (Roles)|
|created|Timestamp|-|-|No|now()|Identifica la fecha de cuando se le asigno un role a un usuario|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización del role de un usuario|

## Group_Role
Tabla pivote entre Groups y Roles con relación de muchos a muchos.

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad Group_Role|
|group_id|Integer|-|UK, FK|No|-|Id del grupo (Groups)|
|role_id|Integer|-|UK, FK|No|-|Id del role (Roles)|
|created|Timestamp|-|-|No|now()|Identifica la fecha de cuando se le asigno un role a un grupo|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización del role de un grupo|

## Resource_Role
Tabla pivote entre Resources y Roles con relación de muchos a muchos.

|**Atribute**|**Type**|**Length**|**Constraint**|**Null**|**Default**|**Description**|
|---|---|---|---|---|---|---|
|id|Serial (Autoincremental)|-|PK|No|-|Clave principal de la entidad Resource_Role|
|resource_id|Integer|-|UK, FK|No|-|Id del recurso (Resources)|
|role_id|Integer|-|UK, FK|No|-|Id del role (Roles)|
|can_add|Boolean|-|-|No|False|Permiso para insertar registros al recurso (Tabla)|
|can_view|Boolean|-|-|No|False|Permiso para consultar resgistros del recurso (Tabla)|
|can_edit|Boolean|-|-|No|False|Permiso para actualizar registros del recurso (Tabla)|
|can_delete|Boolean|-|-|No|False|Permiso para eliminar registros del recurso (Tabla)|
|created|Timestamp|-|-|No|now()|Identifica la fecha cuando a un role se le dieron permisos a un recurso|
|updated|Timestamp|-|-|No|now()|Identifica la fecha de actualización de los permisos de un recurso que tiene un role|